#include "qdp.h"

using namespace QDP;

typedef OSubLattice< PScalar< PColorMatrix< RComplex<REAL>, Nc> > > SubLatticeColorMatrix;

class TimeSliceFunc : public SetFunc
{
public:
  TimeSliceFunc() {}
  int operator() (const multi1d<int>& coordinate) const ;
  int numSubsets() const ;
};


int TimeSliceFunc::operator() (const multi1d<int>& coordinate) const {
  return coordinate[Nd-1];
}

int TimeSliceFunc::numSubsets() const {
  return Layout::lattSize()[Nd-1];
}

void test_tslice() {
  Set tslice;
  tslice.make(TimeSliceFunc());

  LatticeColorMatrix a;
  random(a);

  LatticeColorMatrix b;
  zero_rep(b);

  const int Nt = Layout::lattSize()[Nd-1];

  // Default create a set of Nt SLCV. The default constructor does not allocate memory
  multi1d<SubLatticeColorMatrix> vec1(Nt);
  multi1d<SubLatticeColorMatrix> vec2(Nt);

  for (int i=0; i<Nt; ++i) {
    if(i==2){
      // setting vec1 to random on t=2
      vec1[i].setSubset(tslice[i]);
      zero_rep(vec1[i]); 
      vec1[i] = a;       
      // and vec2 to 0
      vec2[i].setSubset(tslice[i]);
      zero_rep(vec2[i]); 
    } else {
      // on other the everything is random
      vec1[i].setSubset(tslice[i]);
      zero_rep(vec1[i]);
      vec1[i] = a;
 
      vec2[i].setSubset(tslice[i]);
      zero_rep(vec2[i]);
      vec2[i] = a;
    }
  }

  multi1d<Complex> adotvec1(Nt);
  multi1d<Complex> adotvec2(Nt);
  for(int t=0; t<Nt; t++){
    adotvec1[t] = innerProduct(a,vec1[t]);
    adotvec2[t] = innerProduct(a,vec2[t]);
  }

  XMLFileWriter foo("out.xml");
  push(foo,"foo"); 
  write(foo,"adotvec1",adotvec1); 
  write(foo,"adtovec2",adotvec2);
  push(foo,"/foo"); 
  foo.close();
 
}

int main(int argc, char *argv[]) {
  QDP_initialize(&argc, &argv);

  QDPIO::cout<<"Hello to SubLattice"<<std::endl;
  const int foo[] = {2,2,2,4};
  multi1d<int> nrow(Nd);
  nrow = foo;  // Use only Nd elements

  Layout::setLattSize(nrow);
  Layout::create();

  QDPIO::cout<<" test t-slice "<<std::endl;
  test_tslice();

  QDP_finalize();
}



